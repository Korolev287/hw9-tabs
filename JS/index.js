const tabBlock = document.querySelector('.tabs');
const tabArr = document.querySelectorAll('.tabs-title')
const contentArr = document.querySelectorAll('.content')

tabBlock.addEventListener('click', (event)=>{
   tabArr.forEach(element => {
      element.classList.remove('active');
   });
   let tabArticle = event.target.innerHTML;
   event.target.classList.add('active');
   contentArr.forEach(element => {
      element.hidden=true;
      if(tabArticle === element.dataset.name)
      element.hidden=false;
   });
});

